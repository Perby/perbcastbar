-----------------------------------------------------------------------------------------------
-- Client Lua Script for PerbCastbar
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
 
require "Window"
require "GameLib"
require "Spell"
 
-----------------------------------------------------------------------------------------------
-- PerbCastbar Module Definition
-----------------------------------------------------------------------------------------------
local PerbCastbar = {} 
 
-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------
local editMode = 0
 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function PerbCastbar:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self 
    return o
end

function PerbCastbar:Init()
    Apollo.RegisterAddon(self)
end
 

-----------------------------------------------------------------------------------------------
-- PerbCastbar OnLoad
-----------------------------------------------------------------------------------------------
function PerbCastbar:OnLoad()
    -- load our form file
	self.xmlDoc = XmlDoc.CreateFromFile("PerbCastbar.xml")
	self.xmlDoc:RegisterCallback("OnDocLoaded", self)
end

-----------------------------------------------------------------------------------------------
-- PerbCastbar OnDocLoaded
-----------------------------------------------------------------------------------------------
function PerbCastbar:OnDocLoaded()
	if self.xmlDoc == nil then return end
	
	Apollo.LoadSprites("BorderSprite.xml", "Perb")

	Apollo.RegisterSlashCommand("perb", "OnSlashPerb", self)
	Apollo.RegisterEventHandler("WindowManagementReady", "OnWindowManagementReady", self)
	Apollo.RegisterEventHandler("WindowManagementUpdate", "OnWindowManagementUpdate", self)
	Apollo.RegisterEventHandler("StartSpellThreshold", 	"OnStartSpellThreshold", self)
	Apollo.RegisterEventHandler("ClearSpellThreshold", 	"OnClearSpellThreshold", self)
	Apollo.RegisterEventHandler("UpdateSpellThreshold", "OnUpdateSpellThreshold", self)
	Apollo.RegisterTimerHandler("UpdateCastBarTimer", 	"OnUpdate", self)
	
	self.wndCastFrame 		= Apollo.LoadForm(self.xmlDoc, "PerbCastbar", nil, self)
	self.wndTargetCastFrame = Apollo.LoadForm(self.xmlDoc, "TargetCastbar", "FixedHudStratum", self)
	self.wndTapFrame 		= Apollo.LoadForm(self.xmlDoc, "MultiTapBar", "FixedHudStratum", self)
	
	self.cFrameLeft, self.cFrameTop, self.cFrameRight, self.cFrameBottom = self.wndCastFrame:GetAnchorOffsets()
	self.wndTapFrame:SetAnchorOffsets(self.cFrameLeft, self.cFrameTop+9, self.cFrameRight, self.cFrameBottom-16)
	
	self.wndCastFrame:Show(false, true)
	self.wndTargetCastFrame:Show(false, true)
	self.wndTapFrame:Show(false, true)
	self.tCurrentOpSpell = nil
	self.tCurrentOpSpellTap = nil
	
	Apollo.CreateTimer("UpdateCastBarTimer", 0.01, true)
	Apollo.StartTimer("UpdateCastBarTimer")
end

function PerbCastbar:OnWindowManagementReady()
	Event_FireGenericEvent("WindowManagementAdd", {wnd = self.wndCastFrame, strName = "PerbCastbar"})
	Event_FireGenericEvent("WindowManagementAdd", {wnd = self.wndTargetCastFrame, strName = "PerbTargetCastbar"})
end

function PerbCastbar:OnSlashPerb()
	if editMode == 0 then
		self.wndCastFrame:SetStyle("Moveable", true)
		self.wndCastFrame:Show(true, true)
		self.wndTargetCastFrame:SetStyle("Moveable", true)
		self.wndTargetCastFrame:Show(true, true)
		editMode = 1
		Print("PerbCastbar unlocked.")	
	else
		self.wndCastFrame:SetStyle("Moveable", false)
		self.wndCastFrame:Show(false, true)
		self.wndTargetCastFrame:SetStyle("Moveable", false)
		self.wndTargetCastFrame:Show(false, true)
		editMode = 0
		Print("PerbCastbar locked.")
	end
end

function PerbCastbar:OnWindowManagementUpdate(frame)
	if frame and frame.wnd and (frame.wnd == self.wndCastFrame or frame.wnd == self.wndTargetCastFrame) then 
		local bMoveable = frame.wnd:IsStyleOn("Moveable")
		frame.wnd:Show(bMoveable, true)
		frame.wnd:SetStyle("RequireMetaKeyToMove", bMoveable)
		frame.wnd:SetStyle("IgnoreMouse", not bMoveable)
		
		--make frames resizable eventually
		-- frame.wnd:SetStyle("Sizable", bMoveable)
	end
end

function PerbCastbar:OnUpdate()
	local unitPlayer = GameLib.GetPlayerUnit()
	if not unitPlayer then return end
	
	local curLatency = GameLib.GetLatency()
	local unitTarget = unitPlayer:GetTarget()
	local bIsTapping = false
	local myOpSpell = nil

	if self.tCurrentOpSpell ~= nil then
		if self.tCurrentOpSpell.eCastMethod == Spell.CodeEnumCastMethod.RapidTap then
			self:UpdateTapBar(self.wndCastFrame)
		elseif self.tCurrentOpSpell.eCastMethod == Spell.CodeEnumCastMethod.PressHold then
			self:UpdateHoldBar(self.wndCastFrame)
		elseif self.tCurrentOpSpell.eCastMethod == Spell.CodeEnumCastMethod.ChargeRelease then
			self:UpdateChargeBar(self.wndCastFrame)
		end
	else
		local nZone = 0
		local nMaxZone = 0
		local fDuration = 0
		local fElapsed = 0
		local strSpellName = ""
		local nElapsed = 0
		local eType = Unit.CodeEnumCastBarType.None
		
		if unitPlayer:ShouldShowCastBar() or self.wndCastFrame:IsStyleOn("Moveable") then
			self.bIsShown = true
			eType = unitPlayer:GetCastBarType()
			if eType == Unit.CodeEnumCastBarType.Normal then
				bShowCasting = true
				bEnableGlow = true
				nZone = 0
				nMaxZone = 1
				fDuration = unitPlayer:GetCastDuration()
				fElapsed = unitPlayer:GetCastElapsed()

				strSpellName = unitPlayer:GetCastName()
			end
		else
			self.bIsShown = false
			self.wndCastFrame:Show(self.bIsShown, true)
		end	
		
		if bShowCasting and fDuration > 0 and nMaxZone > 0 then
			self.wndCastFrame:Show(bShowCasting, true)

			self.wndCastFrame:FindChild("Castbar"):SetMax(fDuration)
			self.wndCastFrame:FindChild("Castbar"):SetProgress(fElapsed)
			self.wndCastFrame:FindChild("Castname"):SetText(strSpellName)
			self.wndCastFrame:FindChild("Casttime"):SetText(string.format("%00.01f", (fElapsed)/1000))
			
			self.wndCastFrame:FindChild("Latencybar"):SetMax(fDuration)
			self.wndCastFrame:FindChild("Latencybar"):SetProgress(curLatency)
		else --update the latency bar for charge skills
			self.wndCastFrame:FindChild("Latencybar"):SetMax(1000)
			self.wndCastFrame:FindChild("Latencybar"):SetProgress(curLatency)
		end
	end

	if unitTarget ~= nil then
		self:UpdateCastingBar(self.wndTargetCastFrame, unitTarget, "target")
	elseif self.wndTargetCastFrame:IsStyleOn("Moveable") then
		self.wndTargetCastFrame:Show(true, true)
	else
		self.wndTargetCastFrame:Show(false, true)
	end
end

function PerbCastbar:UpdateCastingBar(wndFrame, unitCaster)
	if not unitCaster then return end
	local unitPlayer = GameLib.GetPlayerUnit()
	local dispositionColors = {
							[1]	=	ApolloColor.new("ChannelAdvice"),
							[2]	=	ApolloColor.new("xkcdBoringGreen"),
							[0]	=	ApolloColor.new("AddonError")
							 }
	local dispositionColor = ApolloColor.new(dispositionColors[unitPlayer:GetDispositionTo(unitCaster)])
	
	local nZone = 0
	local nMaxZone = 0
	local fDuration = 0
	local fElapsed = 0
	local strSpellName = ""
	local nElapsed = 0
	local eType = Unit.CodeEnumCastBarType.None
	
	if unitCaster:ShouldShowCastBar() or self.wndTargetCastFrame:IsStyleOn("Moveable") then
		self.bIsShown = true
		eType = unitCaster:GetCastBarType()
		
		if eType == Unit.CodeEnumCastBarType.Normal then
			bShowCasting = true
			bEnableGlow = true
			nZone = 0
			nMaxZone = 1
			fDuration = unitCaster:GetCastDuration()
			fElapsed = unitCaster:GetCastElapsed()
			self.wndTargetCastFrame:FindChild("Castbar"):SetBarColor(dispositionColor)

			strSpellName = unitCaster:GetCastName()
		end
	else
		self.bIsShown = false
		self.wndTargetCastFrame:Show(bIsShown, true)
	end	
	
	if bShowCasting and fDuration > 0 and nMaxZone > 0 then
		self.wndTargetCastFrame:Show(bShowCasting, true)
		self.wndTargetCastFrame:FindChild("Castbar"):SetMax(fDuration)
		self.wndTargetCastFrame:FindChild("Castbar"):SetProgress(fElapsed)
		self.wndTargetCastFrame:FindChild("Castname"):SetText(strSpellName)
		self.wndTargetCastFrame:FindChild("Casttime"):SetText(string.format("%00.01f", (fElapsed)/1000))
		
		--doesn't update back to 0 for some reason
		-- iArmor = unitCaster:GetInterruptArmorValue()
		-- if iArmor ~= 0 then
			-- self.wndTargetCastFrame:FindChild("InterruptArmor"):SetText(iArmor)
		-- end
	end

end

--tesing
function PerbCastbar:OnStartSpellThreshold(idSpell, nMaxThresholds, eCastMethod) -- also fires on tier change
	if self.tCurrentOpSpell ~= nil and idSpell == self.tCurrentOpSpell.id then return end -- we're getting an update event, ignore this one

	self.tCurrentOpSpell = {}
	local splObject = GameLib.GetSpell(idSpell)

	self.tCurrentOpSpell.id = idSpell
	self.tCurrentOpSpell.nCurrentTier = 1
	self.tCurrentOpSpell.nMaxTier = nMaxThresholds
	self.tCurrentOpSpell.eCastMethod = eCastMethod
	self.tCurrentOpSpell.strName = splObject:GetName()

	-- hide all UI elements
	-- self.wndOppBarCircle:Show(false)
	-- self.wndOppBar:Show(false)
	-- for idx = 1, self.tCurrentOpSpell.nMaxTier do
		-- self.tTierMarks[idx]:Show(false)
		-- self.tTierMarks[idx]:FindChild("MarkerBacker"):SetSprite(self.arTierSprites[idx].strMarkEmpty)
		-- self.tTierMarks[idx]:FindChild("Marker"):SetSprite("")
	-- end
	-- self.wndOppBarTiered:Show(false)

	-- restart the progress bar; we'll have to add enum types as they come online
	if self.tCurrentOpSpell.eCastMethod == Spell.CodeEnumCastMethod.RapidTap then
		self.wndCastFrame:Show(true, true)
		self.wndTapFrame:Show(true, true)
	elseif self.tCurrentOpSpell.eCastMethod == Spell.CodeEnumCastMethod.PressHold then
		self.wndCastFrame:Show(true, true)
	elseif self.tCurrentOpSpell.eCastMethod == Spell.CodeEnumCastMethod.ChargeRelease then
		-- for idx = 1, self.tCurrentOpSpell.nMaxTier do
			-- self.tTierMarks[idx]:Show(true)
			-- self.tTierMarks[idx]:FindChild("MarkerBacker"):SetSprite(self.arTierSprites[idx].strMarkEmpty)
			-- self.tTierMarks[idx]:FindChild("Marker"):SetSprite("")
		-- end

		-- self.wndOppBarTiered:FindChild("TierMarkContainer"):ArrangeChildrenHorz(1)
		self.wndCastFrame:Show(true, true)
		-- self.wndOppBarTiered:Show(true)
	end
	self.wndCastFrame:Show(true, true)
	-- self.wndOppFrame:Show(true)

	-- Do the initial update so the first tier is lit up correctly
	self:OnUpdateSpellThreshold(idSpell, self.tCurrentOpSpell.nCurrentTier)
end

function PerbCastbar:OnUpdateSpellThreshold(idSpell, nNewThreshold) -- Updates when P/H/R changes tier or RT tap is performed
	if self.tCurrentOpSpell == nil or idSpell ~= self.tCurrentOpSpell.id then return end
	self.tCurrentOpSpell.nCurrentTier = nNewThreshold
end
--testing

function PerbCastbar:OnClearSpellThreshold(idSpell)
	if self.tCurrentOpSpell ~= nil and idSpell ~= self.tCurrentOpSpell.id then return end -- different spell got loaded up before the previous was cleared. this is valid.

	self.wndTapFrame:Show(false, true)
	-- self.wndOppFrame:Show(false)
	-- self.wndOppBar:Show(false)
	-- self.wndOppBarCircle:Show(false)
	-- self.wndOppBarTiered:Show(false)
	-- self.wndOppBarTiered:FindChild("AlertFlash2"):SetSprite("")
	-- self.wndOppBarCircle:FindChild("AlertFlash"):SetSprite("")
	self.tCurrentOpSpell = nil
	self.wndCastFrame:Show(false, true)
	-- for i = 1, knMaxTiers do
		-- self.tTierMarks[i]:Show(false)
	-- end
end

function PerbCastbar:UpdateHoldBar(wnd)
	local fPercentDone = GameLib.GetSpellThresholdTimePrcntDone(self.tCurrentOpSpell.id)
	wnd:FindChild("Castbar"):SetMax(1)	
	wnd:FindChild("Castbar"):SetProgress(fPercentDone)
	
	-- local strExtra = Apollo.GetString("CastBar_Press")
	-- if Apollo.GetConsoleVariable("spell.useButtonDownForAbilities") then
		-- strExtra = Apollo.GetString("CastBar_Hold")
	-- end
	
	-- wnd:FindChild("Label"):SetText(String_GetWeaselString(Apollo.GetString("CastBar_ComplexLabel"), self.tCurrentOpSpell.strName, strExtra))
end

function PerbCastbar:UpdateTapBar(wnd)
	local fPercentDone = GameLib.GetSpellThresholdTimePrcntDone(self.tCurrentOpSpell.id)
	
	self.wndTapFrame:FindChild("MultiTapTimer"):SetMax(1)
	self.wndTapFrame:FindChild("MultiTapTimer"):SetProgress(1 - fPercentDone)
	self.wndTapFrame:Show(true, true)	
	
	if GameLib.GetPlayerUnit():GetCastDuration() ~= 0 then
		wnd:FindChild("Castname"):SetText(self.tCurrentOpSpell.strName)
		wnd:FindChild("Castbar"):SetMax(GameLib.GetPlayerUnit():GetCastDuration())
		wnd:FindChild("Castbar"):SetProgress(GameLib.GetPlayerUnit():GetCastElapsed())
		if fPercentDone ~= 1 then
			wnd:FindChild("Casttime"):SetText(string.format("%00.01f", (GameLib.GetPlayerUnit():GetCastElapsed())/1000))
		end
		wnd:Show(true, true)
	else
		wnd:FindChild("Castbar"):SetMax(self.tCurrentOpSpell.nMaxTier)
		wnd:FindChild("Castbar"):SetProgress(self.tCurrentOpSpell.nCurrentTier)
		wnd:FindChild("Casttime"):SetText(self.tCurrentOpSpell.nCurrentTier .. "/" .. self.tCurrentOpSpell.nMaxTier)
	end

	-- if fPercentDone ~= 1 then
		-- wnd:FindChild("Casttime"):SetText(string.format("%00.01f", (GameLib.GetPlayerUnit():GetCastElapsed())/1000))
	-- end
end

function PerbCastbar:UpdateChargeBar(wnd)
	local fPercentDone = GameLib.GetSpellThresholdTimePrcntDone(self.tCurrentOpSpell.id)
	-- wnd:FindChild("Fill"):SetFillSprite(self.arTierSprites[self.tCurrentOpSpell.nCurrentTier].strFillSprite)
	-- wnd:FindChild("Fill"):SetGlowSprite(self.arTierSprites[self.tCurrentOpSpell.nCurrentTier].strCapSprite)
	-- wnd:FindChild("Castbar"):SetMax(1)
	wnd:FindChild("Castbar"):SetMax(self.tCurrentOpSpell.nMaxTier - 1)
	-- if self.tCurrentOpSpell.nCurrentTier > 1 then
		-- wnd:FindChild("Castbar"):SetBarColor("xkcdBoringGreen")
		-- wnd:FindChild("FillBacker"):SetSprite(self.arTierSprites[self.tCurrentOpSpell.nCurrentTier - 1].strFillSprite)
	-- else
		-- wnd:FindChild("FillBacker"):SetSprite("")
		-- wnd:FindChild("Castbar"):SetBarColor("xkcdJungleGreen")
	-- end

	-- wnd:FindChild("Fill"):SetMax(1)

	-- if self.tCurrentOpSpell.nCurrentTier == self.tCurrentOpSpell.nMaxTier then
		-- wnd:FindChild("Castbar"):SetProgress(.99) -- last tier would read as empty; this fixes it
		-- wnd:FindChild("Fill"):SetGlowSprite("")
	-- else
		-- wnd:FindChild("Castbar"):SetProgress(fPercentDone)
		wnd:FindChild("Castbar"):SetProgress(fPercentDone + self.tCurrentOpSpell.nCurrentTier - 1)
	-- end
	
	-- local strExtra = Apollo.GetString("DefaultCastBar_Press")
	-- if Apollo.GetConsoleVariable("spell.useButtonDownForAbilities") then
		-- strExtra = Apollo.GetString("DefaultCastBar_Hold")
	-- end

	-- wnd:FindChild("AlertFlash"):Show(self.tCurrentOpSpell.nCurrentTier == self.tCurrentOpSpell.nMaxTier)
	-- wnd:FindChild("Castname"):SetText(String_GetWeaselString(Apollo.GetString("CastBar_ComplexLabel"), self.tCurrentOpSpell.strName, strExtra)) -- todo: Set the name once we have a function for it
	wnd:FindChild("Castname"):SetText(self.tCurrentOpSpell.strName)
	-- wnd:FindChild("Casttime"):SetText(string.format("%00.01f", (GameLib.GetPlayerUnit():GetCastElapsed())/1000))
	if fPercentDone ~= 1 then
		wnd:FindChild("Casttime"):SetText(string.format("%00.01f", (GameLib.GetPlayerUnit():GetCastElapsed())/1000))
	end
end

-----------------------------------------------------------------------------------------------
-- PerbCastbar Instance
-----------------------------------------------------------------------------------------------
local PerbCastbarInst = PerbCastbar:new()
PerbCastbarInst:Init()
